<?php

namespace BuyPlanEstonia\BuyPlanPayment\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use BuyPlanEstonia\BuyPlanPayment\Model\BuyPlanConstants;
use BuyPlanEstonia\BuyPlanPayment\Model\ConfigFields;
use BuyPlan\Payment\lib\BuyPlan\BuyPlan;

class Data extends AbstractHelper
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Data constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    )
    {
        parent::__construct($context);
        $this->scopeConfig = $context->getScopeConfig();
    }

    /**
     * @param string $field
     * @return mixed
     */
    public function getConfig(string $field) {
        return $this->scopeConfig->getValue("payment/buyplan/{$field}", ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return BuyPlan
     */
    public function getBuyPlan() {
        return new BuyPlan(
            $this->getConfig(ConfigFields::MERCHANT_PRIVATE_KEY),
            $this->getConfig(ConfigFields::BUYPLAN_PUBLIC_KEY),
            $this->getConfig(ConfigFields::BUYPLAN_API_URL),
            $this->getConfig(ConfigFields::MERCHANT_REGISTRY_CODE),
            $this->getConfig(ConfigFields::MERCHANT_PRIVATE_KEY_PASSWORD)
        );
    }
}
