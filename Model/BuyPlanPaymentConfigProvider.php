<?php

namespace BuyPlanEstonia\BuyPlanPayment\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Session\SessionManagerInterface;
use BuyPlanEstonia\BuyPlanPayment\Model\ConfigFields;
use BuyPlanEstonia\BuyPlanPayment\Logger\Logger;
use BuyPlanEstonia\BuyPlanPayment\Helper\Data;
use BuyPlan\Payment\lib\BuyPlan\util\Util;

class BuyPlanPaymentConfigProvider implements ConfigProviderInterface
{
    protected $helper;
    protected $logger;

    public function __construct(Data $helper, Logger $logger) {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->logger->info('Constructed the Config Provider...');
    }

    public function getConfig() {

        $configuredButtonTitle = $this->helper->getConfig(ConfigFields::PAYMENT_BUTTON_TITLE);

        $configuredButtonImageURL = $this->helper->getConfig(ConfigFields::PAYMENT_BUTTON_IMAGE_URL);
        $defaultButtonImageURL = BuyPlanConstants::DEFAULT_BUTTON_IMAGE_URL;
        if (!Util::isBuyPlanImageURL($configuredButtonImageURL)) {
            $configuredButtonImageURL = $defaultButtonImageURL;
        }

        $configuredButtonMaxHeight = $this->helper->getConfig(ConfigFields::PAYMENT_BUTTON_MAX_HEIGHT);
        $defaultButtonMaxHeight = BuyPlanConstants::DEFAULT_BUTTON_MAX_HEIGHT;
        if ($configuredButtonMaxHeight < 32 || $configuredButtonMaxHeight > 512) {
            $configuredButtonMaxHeight = $defaultButtonMaxHeight;
        }

        $config = [];
        $config['BPButtonTitle'] = $configuredButtonTitle;
        $config['BPButtonImageURL'] = $configuredButtonImageURL;
        $config['BPButtonMaxHeight'] = strval($configuredButtonMaxHeight).'px';

        $this->logger->info('Returning BuyPlan Payment Config...');
        return $config;
    }
}
