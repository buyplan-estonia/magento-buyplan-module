<?php

namespace BuyPlanEstonia\BuyPlanPayment\Model;

final class BuyPlanConstants {
    const DEFAULT_BUTTON_IMAGE_URL = 'https://docs.buyplan.ee/brand-guidelines/payment_button_dark.svg';
    const DEFAULT_BUTTON_MAX_HEIGHT = 100;
}