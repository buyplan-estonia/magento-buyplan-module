<?php

namespace BuyPlanEstonia\BuyPlanPayment\Model;

final class ConfigFields {
    const BUYPLAN_API_URL = 'buyplan_api_url';
    const BUYPLAN_PUBLIC_KEY = 'buyplan_public_key';
    const MERCHANT_REGISTRY_CODE = 'merchant_registry_code';
    const MERCHANT_PRIVATE_KEY = 'merchant_private_key';
    const MERCHANT_PRIVATE_KEY_PASSWORD = 'merchant_private_key_password';
    const BUYPLAN_MIN_CART_AMOUNT = 'buyplan_minimum_cart_amount';
    const PAYMENT_BUTTON_TITLE = 'payment_button_title';
    const PAYMENT_BUTTON_IMAGE_URL = 'payment_button_image_url';
    const PAYMENT_BUTTON_MAX_HEIGHT = 'payment_button_max_height';
}
