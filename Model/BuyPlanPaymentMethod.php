<?php

namespace BuyPlanEstonia\BuyPlanPayment\Model;

use Magento\Payment\Model\Method\AbstractMethod;

class BuyPlanPaymentMethod extends AbstractMethod
{
    protected $_isInitializeNeeded = false;
    protected $_code = 'buyplan';
    protected $_canOrder = true;
    protected $_isGateway = true;
}
