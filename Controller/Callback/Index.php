<?php
namespace BuyPlanEstonia\BuyPlanPayment\Controller\Callback;

use BuyPlanEstonia\BuyPlanPayment\Helper\Data;
use BuyPlan\Payment\lib\BuyPlan\domain\Response;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Session;
use Magento\Sales\Model\Order;
use BuyPlanEstonia\BuyPlanPayment\Logger\Logger;
use Magento\Sales\Model\Order\Payment\Transaction\Builder as TransactionBuilder;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Helper\Data as CheckoutData;
use Magento\Sales\Api\TransactionRepositoryInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Sales\Model\OrderNotifier;
use Magento\Sales\Model\Service\InvoiceService;

class Index extends Action
{

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var TransactionBuilder
     */
    protected $transactionBuilder;

    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var TransactionRepositoryInterface
     */
    protected $transactionRepository;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var InvoiceService
     */
    protected $invoiceService;

    /**
     * @var OrderNotifier
     */
    protected $orderNotifier;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var CheckoutData
     */
    protected $checkoutHelper;

    /**
     * Index constructor.
     * @param Context $context
     * @param Session $checkoutSession
     * @param OrderRepositoryInterface $orderRepository
     * @param Logger $logger
     * @param TransactionBuilder $transactionBuilder
     * @param Cart $cart
     * @param TransactionRepositoryInterface $transactionRepository
     * @param InvoiceService $invoiceService
     * @param OrderNotifier $orderNotifier
     * @param Data $helper
     * @param CheckoutData $checkoutHelper
     */
    public function __construct( Context $context,
                                 Session $checkoutSession,
                                 OrderRepositoryInterface $orderRepository,
                                 Logger $logger,
                                 TransactionBuilder $transactionBuilder,
                                 Cart $cart,
                                 TransactionRepositoryInterface $transactionRepository,
                                 InvoiceService $invoiceService,
                                 OrderNotifier $orderNotifier,
                                 Data $helper,
                                 CheckoutData $checkoutHelper
    ) {

        $this->checkoutSession = $checkoutSession;
        $this->orderRepository = $orderRepository;
        $this->transactionBuilder = $transactionBuilder;
        $this->logger = $logger;
        $this->cart = $cart;
        $this->transactionRepository = $transactionRepository;
        $this->invoiceService = $invoiceService;
        $this->orderNotifier = $orderNotifier;
        $this->helper = $helper;
        $this->checkoutHelper = $checkoutHelper;

        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $response = $this->getRequest()->getPost();

        if (!$response || empty($response)) {
            return;
        }

        $buyplan = $this->helper->getBuyPlan();

        try {
            $paymentResponse = $buyplan->getResponse($response);
        } catch (\InvalidArgumentException $e) {
            $this->logger->debug('An error occurred while processing the response.');
            return;
        }

        $orderId = $paymentResponse->getOrderId();
        $order = $this->orderRepository->get($orderId);
        $payment = $order->getPayment();

        if($order && $payment && $paymentResponse->getStatus() !== Response::STATUS_ERROR) {
            if ($payment->getMethod() == 'buyplan' && $paymentResponse->isSuccesful()) {
                // if the condition below is true, this means callback indeed reached first and all normal
                if ($order->getState() == Order::STATE_PENDING_PAYMENT) {

                    $order->setState(Order::STATE_PROCESSING);
                    $order->setStatus($order->getConfig()->getStateDefaultStatus(Order::STATE_PROCESSING));

                    $invoice = $this->invoiceService->prepareInvoice($order);
                    $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
                    $invoice->register();

                    $order->setCanSendNewEmailFlag(true);
                    $this->orderNotifier->notify($order);

                    $invoice->pay();
                    $invoice->save();
                    $order->save();

                    $this->logger->debug("Payment successful.");
                } else {
                    // this part potentially needs refactoring in future, discussed but held back

                    if (!$order->hasInvoices()) {
                        $invoice = $this->invoiceService->prepareInvoice($order);
                        $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
                        $invoice->register();

                        $order->setCanSendNewEmailFlag(true);
                        $this->orderNotifier->notify($order);

                        $invoice->pay();
                        $invoice->save();
                        $order->save();
                    }

                    $this->logger->debug("Payment successful, but unexpected order status.");
                }
            } else {
                // restoring the shopping cart
                try {
                    $this->checkoutHelper->getCheckout()->restoreQuote();
                } catch (\Exception $e){
                    $this->logger->debug("An error occurred while restoring the cart for cancelled payment.");
                }

                $order->cancel();
                $order->save();
                $this->logger->debug("Payment cancelled.");
            }
        } else {
            $this->logger->debug('An error occurred while processing the payment.');
            return;
        }
    }
}