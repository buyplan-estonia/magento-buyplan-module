<?php

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'BuyPlanEstonia_BuyPlanPayment', __DIR__);
