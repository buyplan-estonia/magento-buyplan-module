<?php

namespace BuyPlanEstonia\BuyPlanPayment\Observer;

use BuyPlanEstonia\BuyPlanPayment\Helper\Data;
use BuyPlanEstonia\BuyPlanPayment\Model\ConfigFields;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use \Magento\Checkout\Model\Session as CheckoutSession;

class DisablePaymentMethod implements ObserverInterface
{
    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var Data
     */
    protected $helper;

    public function __construct(
        CheckoutSession  $checkoutSession,
        Data $helper
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->helper = $helper;
    }

    public function execute(Observer $observer)
    {
        $method = $observer->getEvent()->getMethodInstance();

        if ($method && $method->getCode() == 'buyplan') {

            $quote = $this->checkoutSession->getQuote();
            $purchaseMinLimit = $this->helper->getConfig(ConfigFields::BUYPLAN_MIN_CART_AMOUNT);

            if ($quote && $quote->getGrandTotal() < $purchaseMinLimit) {
                $result = $observer->getEvent()->getResult();
                $result->setData('is_available', false);
            }
        }
    }
}