<?php
namespace BuyPlanEstonia\BuyPlanPayment\Block;

use BuyPlanEstonia\BuyPlanPayment\Helper\Data;
use BuyPlanEstonia\BuyPlanPayment\Logger\Logger;
use BuyPlanEstonia\BuyPlanPayment\Model\ConfigFields;
use BuyPlan\Payment\lib\BuyPlan\config\Config;
use BuyPlan\Payment\lib\BuyPlan\domain\Request;
use BuyPlan\Payment\lib\BuyPlan\domain\ClientInfo;
use BuyPlan\Payment\lib\BuyPlan\domain\OrderRow;
use BuyPlan\Payment\lib\BuyPlan\domain\Order as BuyPlanOrder;
use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Checkout\Model\Session;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Order\Payment\Transaction\Builder as TransactionBuilder;


class Main extends \Magento\Framework\View\Element\Template
{

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @var TransactionBuilder
     */
    protected $transactionBuilder;

    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @var RedirectInterface
     */
    protected $redirect;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * Main constructor.
     * @param Context $context
     * @param Session $checkoutSession
     * @param OrderFactory $orderFactory
     * @param Logger $logger
     * @param TransactionBuilder $transactionBuilder
     * @param ResponseInterface $response
     * @param RedirectInterface $redirect
     * @param Cart $cart
     * @param Data $helper
     */
    public function __construct(Context $context,
                                Session $checkoutSession,
                                OrderFactory $orderFactory,
                                Logger $logger,
                                TransactionBuilder $transactionBuilder,
                                ResponseInterface $response,
                                RedirectInterface $redirect,
                                Cart $cart,
                                Data $helper
    ) {

        $this->checkoutSession = $checkoutSession;
        $this->orderFactory = $orderFactory;
        $this->transactionBuilder = $transactionBuilder;
        $this->response = $response;
        $this->redirect = $redirect;
        $this->cart = $cart;
        $this->logger = $logger;
        $this->helper = $helper;

        parent::__construct($context);
    }

    /**
     * @return Main|void
     * @throws \Exception
     */
    protected function _prepareLayout()
    {
        $this->logger->debug('Starting BuyPlan Payment Redirect...');

        $failureUrl = $this->_urlBuilder->getUrl('checkout/cart',  ['_secure' => true]);

        $orderId = $this->checkoutSession->getLastOrderId();
        $order = $this->orderFactory->create()->load($orderId);

        // this is important for correct async and sync payment response processing
        $order->setState(Order::STATE_PENDING_PAYMENT);
        $order->setStatus($order->getConfig()->getStateDefaultStatus(Order::STATE_PENDING_PAYMENT));

        if ($order->getId()) {
            $order->save();
            try {
                $buyplan = $this->helper->getBuyPlan();

                $request = new Request(
                    $this->helper->getConfig(ConfigFields::MERCHANT_REGISTRY_CODE),
                    $order->getId(),
                    $order->getGrandTotal(),
                    $order->getId(),
                    '',
                    $this->_urlBuilder->getUrl('buyplan/response'),
                    $this->_urlBuilder->getUrl('buyplan/response'),
                    $this->getBuyPlanOrder($order),
                    $this->_urlBuilder->getUrl('buyplan/callback')
                );

                $this->setBuyPlanHtml($buyplan->getRequestForm($request));
            }
            catch (\Exception $e) {
                $this->logger->debug("An error occurred while initiating the BuyPlan payment request.");
                $this->tryToRestoreCartAndRedirect($order, $failureUrl);
            }
        } else {
            $this->logger->debug("Order not found.");
            $this->redirect->redirect($this->response, $failureUrl);
        }
        return;
    }

    /**
     * @param Order $order
     * @param string $failureUrl
     * @throws \Exception
     */
    private function tryToRestoreCartAndRedirect($order, $failureUrl)
    {
        try {
            $items = $order->getItemsCollection();
            foreach($items as $item)
                $this->cart->addOrderItem($item);
            $this->cart->save();
            $order->cancel();
            $order->save();
        } catch(\Exception $e){
            $this->logger->debug("An error occurred while restoring the cart for the redirect.");
        }
        $this->redirect->redirect($this->response, $failureUrl);
    }

    private function getBuyPlanOrder(Order $order)
    {
        $billing = $order->getBillingAddress();

        $clientInfo = new ClientInfo(
            $billing->getFirstname(),
            $billing->getLastname(),
            $billing->getEmail(),
            $billing->getTelephone(),
            $billing->getCountryId(),
            implode(', ', $billing->getStreet()) ?: '',
            $billing->getPostcode()
        );

        $orderRows = [];

        foreach ($order->getAllVisibleItems() as $item) {
            $orderItem = new OrderRow(
                $item->getName() ?: '',
                $item->getDescription() ?: '',
                $item->getPriceInclTax() ?: '',
                (int)$item->getQtyOrdered()
            );

            $orderRows[] = $orderItem;
        }

        $orderRows[] = new OrderRow(
            'Postikulu',
            $order->getShippingDescription() ?: '',
            $order->getShippingAmount() ?: '',
            1
        );

        return new BuyPlanOrder($clientInfo, $orderRows, Config::MIN_PAYMENT_LENGTH);
    }
}
