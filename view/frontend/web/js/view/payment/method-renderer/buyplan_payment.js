/*browser:true*/
/*global define*/
define(
    [
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/action/redirect-on-success',
        'mage/url'
    ],
    function (Component, redirectOnSuccessAction, url) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'BuyPlanEstonia_BuyPlanPayment/payment/buyplan_payment'
            },

            afterPlaceOrder: function () {
                redirectOnSuccessAction.redirectUrl = url.build('buyplan/redirect');
                this.redirectAfterPlaceOrder = true;
            }
        });
    }
);
