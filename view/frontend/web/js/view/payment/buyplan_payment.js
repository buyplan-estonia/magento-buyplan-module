/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'buyplan',
                component: 'BuyPlanEstonia_BuyPlanPayment/js/view/payment/method-renderer/buyplan_payment'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);
